# Kionix Sensor Drivers for Linux Kernel/Android #

## Accelerometer Drivers ##

### [KXCJK-1013](http://www.kionix.com/product/KXCJK-1013), [KXCJ9-1008](http://www.kionix.com/product/KXCJ9-1018), [KXTJ2-1009](http://www.kionix.com/product/KXTJ2-1009) ###

* **Driver Type:** Industrial I/O accelerometer, I2C
* **Driver Status:** Linux kernel
* [source codes link](http://lxr.free-electrons.com/source/drivers/iio/accel/kxcjk-1013.c)


### [KX022-1020](http://www.kionix.com/product/KX022-1020), [KX023-1025](http://www.kionix.com/product/KX023-1025), [KX112-1042](http://www.kionix.com/product/KX112-1042), [KX122-1037](http://www.kionix.com/product/KX122-1037), [KX123-1039](http://www.kionix.com/product/KX123-1039), [KX124-1050](http://www.kionix.com/product/KX124-1050) ###

* **Driver Status:** development ongoing

##  6-axis Accelerometer/Gyroscope Drivers ##

### [KXG03](http://www.kionix.com/product/KXG03) ###

* **Driver Type:** Input system misc, I2C
* **Driver Status:** Android 5.1 CTS testing pass
* source codes this repository

### [KXG07](http://www.kionix.com/product/KXG07) ###

* **Driver Status:** development ongoing

### [KXG08](http://www.kionix.com/product/KXG08) ###

* **Driver Status:** development ongoing

##  6-axis Accelerometer/Magnetometer Drivers ##

### [KMX62](http://www.kionix.com/product/KMX62) ###

* **Driver Type:** Input system misc, I2C
* **Driver Status:** development done, testing ongoing
* source codes this repository

## About Android CTS Sensor Testing ##

### Android 5.1 CTS Testing ###

Android CTS sensor testing was done on Qualcomm Dragonboard 410c device running [Android 5.1/Linux Kernel 3.10](https://developer.qualcomm.com/hardware/dragonboard-410c)