# Kionix KMX62 Tri-Axis Magnetometer/ Tri-Axis Accelerometer driver for Linux kernel #

[Kionix KMX62 Tri-Axis Magnetometer/ Tri-Axis Accelerometer](http://www.kionix.com/product/KMX62)

KMX62 Linux kernel driver uses I2C bus to communicate with sensor. Driver uses input device to report sensor data to user space. One input device is allocated for magnetometer and one for accelerometer.

# Driver configuration #

** NOTE, detailed configuration info will be added later. KMX62 driver is configured in a same way as KXG03. So for further info see details from KXG03 driver configuration.**


**Example** device tree configuration

Sensor i2c configuration for Dragon Board 410c

```
kernel/arch/arm/boot/dts/qcom/apq8016-sbc.dtsi

		kmx62@f { /* Kionix accelerometer/magnetometer combo */
			compatible = "kionix,kmx62";
			reg = <0x0f>;
			pinctrl-names = "kmx62_default", "kmx62_sleep";
			pinctrl-0 = <&kmx62_default>;
			pinctrl-1 = <&kmx62_sleep>;
			interrupt-parent = <&msm_gpio>;
			interrupts = <69 0x1>;
			kionix,gpio-int2 = <&msm_gpio 69 0x1>;
			kionix,x-map = <0>;
			kionix,y-map = <1>;
			kionix,z-map = <2>;
			kionix,x-negate = <0>;
			kionix,y-negate = <0>;
			kionix,z-negate = <0>;
			kionix,g_range = <8>;
			kionix,use-drdy-int;
		};

```
Sensor gpio interrupt pincontrol configuration:
```
kernel/arch/arm/boot/dts/qcom/msm8916-pinctrl.dtsi

		kmx62_int_pin {
			qcom,pins = <&gp 69>;
			qcom,pin-func = <0>;
			qcom,num-grp-pins = <1>;
			label = "kmx62-irq2";
			kmx62_default: kmx62_default {
				drive-strength = <2>;
				bias-disable = <0>;	/* No PULL */
			};
			kmx62_sleep: kmx62_sleep {
				drive-strength = <2>;
				bias-disable = <0>;	/* No PULL */
			};
		};

